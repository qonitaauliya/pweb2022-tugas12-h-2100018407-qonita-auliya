<?php

$arrNilai = array("Alya"=>85, "Audila"=>80, "layla"=>90, "raya"=>75);
echo "Menampilkan isi array asosiatif dengan foreach : <br>";
foreach ($arrNilai as $nama => $nilai) {
	echo "Nilai $nama = $nilai<br>";
}

reset($arrNilai);
echo "Menampilkan isi array asosiatif dengan WHILE dan LIST : <br>";
while (list($nama, $nilai) = each($arrNilai)) {
	echo "Nilai $nama = $nilai<br>";
}
?>